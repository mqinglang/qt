﻿#include <QSerialPortInfo>
#include <QSerialPort>
#include <QDateTime>
#include "frmmain.h"
#include "ui_frmmain.h"
#include "serial.h"
FrmMain::FrmMain(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::FrmMain)
{
    ui->setupUi(this);

    initWidget();
    intData();
}

FrmMain::~FrmMain()
{
    delete ui;
}

void FrmMain::initWidget()
{
    //初始化串口设置
    //1.获得串口号
    QStringList serialList = getAvalibSerialPort();
    if(serialList.size()<=0)
        serialList<<"NULL";
    ui->cbxPortName->addItems(serialList);
    //2.波特率
    QStringList baudList;
    baudList<<"1200"<<"2400"<<"4800"<<"9600"<<"19200"<<"38400"<<"57600"<<"115200";
    ui->cbxBaud->addItems(baudList);
    //3.数据位
    QStringList dataList;
    dataList<<"5"<<"6"<<"7"<<"8";
    ui->cbxData->addItems(dataList);
    //4.奇偶
    QStringList parityList;
    parityList<<"0"<<"2"<<"3"<<"4"<<"5";
    ui->cbxParity->addItems(parityList);
    //5.停止位
    QStringList stopList;
    stopList<<"1"<<"2"<<"3";
    ui->cbxStop->addItems(stopList);
    //6.流控制
    QStringList flowControlList;
    flowControlList<<"0"<<"1"<<"2";
    ui->cbxFlowControl->addItems(flowControlList);

    ui->cbxBaud->setCurrentIndex(7);
    ui->cbxData->setCurrentIndex(3);
    ui->cbxParity->setCurrentIndex(0);
    ui->cbxStop->setCurrentIndex(1);
    ui->cbxFlowControl->setCurrentIndex(0);
    ui->btnOPen->setEnabled(true);
    ui->btnClose->setEnabled(false);
    ui->frame->setEnabled(true);

    ui->textEdit->document()->setMaximumBlockCount(100);
}

void FrmMain::intData()
{
    connect(Serial::Insatnce(),SIGNAL(msgTip(int,QString)),this,SLOT(showMsg(int,QString)));
}

QStringList FrmMain::getAvalibSerialPort()
{
    QStringList serialPortNames;
    QSerialPort serial;
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
    for(const QSerialPortInfo &port : ports){
        serial.setPortName(port.portName());
        if(serial.open(QIODevice::ReadWrite))
        {
            serial.close();
        }
        serialPortNames<<port.portName();
    }
    return serialPortNames;
}

void FrmMain::showMsg(int type, const QString &msg)
{
    QString currentTime = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
    QString content = "";
   if(type == 0)
    {
        content = currentTime+" 收到数据<< "+msg;
        ui->textEdit->setTextColor(QColor(Qt::red));
    }
    else if(type == 1)
    {
        content = currentTime+" 发送数据>> "+msg;
        ui->textEdit->setTextColor(QColor(Qt::blue));
    }
    else if(type == 2)
    {
        content = currentTime+" --提示--："+msg;
        ui->textEdit->setTextColor(QColor(Qt::darkBlue));
    }

    ui->textEdit->append(content);
}


void FrmMain::on_btnOPen_clicked()
{
    serialInfo info;
    info.portName = ui->cbxPortName->currentText();
    info.baudRate = ui->cbxBaud->currentText();
    info.dataBit = ui->cbxData->currentText();
    info.parity = ui->cbxParity->currentText();
    info.stopBit = ui->cbxStop->currentText();
    info.flowControl = ui->cbxFlowControl->currentText();

    if(Serial::Insatnce()->isOpen(info))
    {
        ui->btnOPen->setEnabled(false);
        ui->btnClose->setEnabled(true);
    }
    else
    {
        ui->btnOPen->setEnabled(true);
        ui->btnClose->setEnabled(false);
    }
}


void FrmMain::on_btnClose_clicked()
{
    Serial::Insatnce()->close();
    ui->btnOPen->setEnabled(true);
    ui->btnClose->setEnabled(false);
}


void FrmMain::on_btnSend_clicked()
{
    QString data = ui->txtSendData->text();
    Serial::Insatnce()->onSendData(data);
}

