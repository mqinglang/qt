﻿#ifndef SERIAL_H
#define SERIAL_H


#include <QObject>
#pragma execution_character_set("utf-8")

struct serialInfo
{
    QString portName;    //串口号
    QString baudRate;    //波特率
    QString dataBit;     //数据位
    QString parity;      //奇偶校验
    QString stopBit;     //停止位
    QString flowControl; //流控制
};

class QSerialPort;
class Serial : public QObject
{
    Q_OBJECT
public:
    static Serial*Insatnce();
    explicit Serial(QObject *parent = nullptr);
    void init();
    //QByteArray转换成16进制QString
    QString byteArrayToHexStr(const QByteArray &data);
    //16进制QString转换成QByteArray
    QByteArray hexStrToByteArray(const QString &data);
    char hexStrToChar(char data);

signals:
    //0-recv 1-send 2-tip
    void msgTip(int type,QString msg);
public slots:
    bool isOpen(const serialInfo &info);
    void close();
    //读取串口
    void onReadyRead();
    void onSendData(const QString &data);
private:
    static QScopedPointer<Serial> self;
    QSerialPort *serialCom;              //串口类
};

#endif // SERIAL_H
