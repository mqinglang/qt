﻿
#ifndef FRMMAIN_H
#define FRMMAIN_H

#include <QMainWindow>
#pragma execution_character_set("utf-8")
QT_BEGIN_NAMESPACE
namespace Ui { class FrmMain; }
QT_END_NAMESPACE

class FrmMain : public QMainWindow
{
    Q_OBJECT

public:
    FrmMain(QWidget *parent = nullptr);
    ~FrmMain();

    void initWidget();
    void intData();
    //获得可用串口号
    QStringList getAvalibSerialPort();

private slots:
    void showMsg(int type,const QString &msg);
    void on_btnOPen_clicked();
    void on_btnClose_clicked();

    void on_btnSend_clicked();

private:
    Ui::FrmMain *ui;
};

#endif // FRMMAIN_H
